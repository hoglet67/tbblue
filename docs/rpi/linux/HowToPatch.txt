The patching of NextPi is done via the NextPiUI dot command, or a USB stick.

USB Method
==========

 * Ensure your USB key is FAT formatted and contains only one partition.
 * Copy the .PUP file to the root the drive, renaming it to UPDATE.PUP
 * Ensure there is no file named "UPDATE.LOG" in the root of the USB drive, if
   there is remove it otherwise the update will not install.
 * Remove the HDMI and power from the next, and all (if any) attached cables
   from the raspberry pi.
 * Plug the USB key, via a OSB OTG adapter if required, into the micro USB port
   labelled USB (when facing the rear of the machine it is the left hand micro
   USB port - the one nearest the HDMI port/above the EARMIC socket.
 * Attach an HDMI monitor via a Mini-HDMI cable/adapter to the "Digital Video
   Debug" port, so you can monitor the boot/patching process.

 Once the message "Please login to continue" appears the upgrade process is
 completed. If you also have a keyboard attached (via a hub) you can use SHIFT
 and PAGE-UP to scroll the messages and see any errors, or other information
 logs.  You can also find a textlog of the update process saved in "update.log"
 on the root of the USB key.


Next Hosted Method
==================
 * Extract the contents of the .zip file to a folder on your next.  You can do
   this easily by creating an empty folder on your next, copying the .zip file
   to it, and then opening the .zip file with the browser which will extract it.
 * From the "Command Line" mode of your next run ".nextpiui" and confirm you can
   connect successfully to your Pi.  This procedure will not work unless this
   stage completes successfully.  If NextPiUi starts without any warnings of
   mismatched versions, you can use "Method A" below to upgrade your Pi, if it
   does not report a matched version, you should proceed via "Method B".  It is
   also important to check that the patch you have downloaded is the correct
   version for the current version of NextPi you're running, as reported by
   NextPiUI.  A mismatched patch version will just fail to install, so you can
   shortcut the time it takes for that to happen by checking the version printed
   at the top of the readme.

Method A
--------
 * Navigate to the .pup file and hit enter to open the file from the browser.
 * Follow the on-screen prompts - NextPi should update automatically.
 Once complete, and NextPiUI quits, proceed to the "Final Stages" section.

Method B
--------
 * From "Command Line" mode, while in the directory you extracted the update in
   start the local copy (in the folder you just extracted) of .NextPiUI with the
   command:
        ../nextpiui <NameOfPatch>.pup
 * Follow the on-screen prompts - NextPi should update automatically.
 Once complete, and NextPiUI quits, proceed to the "Final Stages" section.


Final Stages
============
 These steps apply to both the USB, and Next Hosted update methods

 * Copy the NextPiUI file from the "extras" folder in the zip file to /dot on
   your SD card.
 * From "Command Line" run the updated interface with ".NextPiUI" to confirm the
   new version of NextPi2 that is running on your Pi.