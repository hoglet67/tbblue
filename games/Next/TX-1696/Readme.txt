TX-1696  DEMO
  
Stardate 1696 , The Taris Xavia has been sent out into deep unknown space after a signal was intercepted on TOI 700e, the only known human inhabited planet to exist. 

After intercepting the strange alien signal you are met by hostile lifeforms that are determined to obliterate you. Your onboard computer deciphers the signal and shows the alien creatures intentions. Humans must die. you pull out of warp and get ready to use your powerful elite experimental weapon systems to take out the massive threat that awaits. 

Good luck commander the human race depends on you.


TX-1696 pushes the Spectrum Next to its limits and features massive boss baddies, horizontal and vertical play areas, masses of highly detailed sprites on screen and loads of weapon pickups with a unique weapon selection system. 
We also get a taste of some fantastic techno style music from Richard Faulkner aka A man in his Techno shed  

The games graphics and code have been produced by Patricia Curtis who has worked on some huge titles like the conversion of Xenon 2 for the Amiga CDTV and Megadrive!

TX-1696 is the ultimate Spectrum Next game that you need for your collection

You can show your interest and pre-order TX-1696 from the following website: 
https://luckyredfish.com/priority-order-list-for-tx-1696/


TX-1696 supports gamepads and Keyboard controls only
(Tested with a kempston sega mega drive style pad & a Playstation 4 pad using an 8bitDo Bluetooth device)

Controls

Gamepad
A = back (front menu)
B = Fire (hold for beam)
C / (circle) = Weapons selection, you shoot the weapon’s case to open it, it will display a (?) then press and hold button C to select the weapons using the direction arrows. Once you have selected you weapon of choice simply fly into your selection.


KEYS

Space = fire
Z/enter = weapon select
X = back
H = paused
P/cursor right = right
O/cursor left = left
Q/cursor up = up
A/cursor down = down

The game does work with Auto fire however to get the best enjoyment out of it use the beam option. 

Enjoy the TX-1696 Team
